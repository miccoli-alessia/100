//initialization
//first step: check if the visitor is new.
var maxC;

if(document.cookie == ""){
    maxC = 5;
    setCookie("maxC", maxC, 365);
}
else{
    maxC = getCookie("maxC");
    
}

//rest of the variables to be initialized
var counter = 0;
var oldId = 00;
var moves = new Array();
var win;
var lost;

function settingsTable(){
    var MAXN = maxC * maxC;
    var maxR = maxC;
}

settingsTable();

//set Cookie Function -> cname = name of the cookie, cvalue = value of the cookie, exdays = number of days in which
//the cookie will expire

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
} 

//get Cookie Function -> if the cookie is found return its value, if not found return ""

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            var x = c.substring(name.length, c.length);
            return parseInt(x);
        }
    }
    return "";
}

//first thing loaded : two buttons that give you the choise to:
// 1 - start the game
// 2 - show you the rules of the game

var chooseButton = function chooseButton(){
    var body = document.getElementsByTagName('body')[0];
    var bannerMobile = document.getElementById('banner-mobile');
    var bannerPc = document.getElementById('banner-pc');
    var buttons = document.createElement('div');
    var start = document.createElement('button');
    var howToPlay = document.createElement('button');
    buttons.setAttribute("id","buttons");
    body.appendChild(buttons);
    start.setAttribute("id","home");
    howToPlay.setAttribute("id","home");
    start.innerHTML = "Start";
    howToPlay.innerHTML = "How To Play";
    buttons.appendChild(start);
    buttons.appendChild(howToPlay);

    start.onclick = () =>{
        buttons.remove();
        createTable(body);
        if(screen.width < 450){
            bannerMobile.style.display = "block";
            bannerPc.style.display = "none";
        }
        else{
            bannerMobile.style.display = "none";
            bannerPc.style.display = "block";
        }
    }

    howToPlay.onclick = ()=>{
        howToP();
    }
}

window.onload = chooseButton;

function howToP(){
    var modal = document.getElementById("modal");
    modal.style.display = "block";
}

//creation of the table
var createTable = function tableCreate(body) {
    var table = document.createElement('table');
    var message = document.createElement('p');
    win = false;
    lost = false;

    message.style.display = "none";
    body.appendChild(message);
    message.setAttribute("id","message");

    for(var i=0; i<maxC; i++){
        var row = document.createElement('tr');
        for(var j=0; j<maxC; j++){
            var column = document.createElement('td');
            column.style.height = table.height/maxC;
            column.style.width = table.width/maxC;
            table.appendChild(column);
            //the value of the current row and column is assigned to the id of the cell created.
            column.setAttribute("id",`${i}${j}`);
            //every cell is inizialized to the value "X".
            column.innerHTML = '<p style="color: transparent">X</p>';  

            //when you click on a cell, if it's the first move (counter equal to 0), you insert the cell
            //directly and return the current id value. If it's not the first move (counter not equal to 0)
            //the function "checkCell" is called.   
            column.onclick = function(){
                if(counter === 0){
                    oldId = insertNumber(this.id);
                    possibleMoves(this.id);
                    document.getElementById(this.id).style.border = "1px solid green";
                    resetButton("Restart", body, table);
                }
                else{
                    oldId = checkCell(this.id, oldId, table, body, message);
                }
            }
        }
        row.style.height = "0";
        table.appendChild(row);
    }

    body.appendChild(table);
    table.setAttribute("border", "1px solid black");
}

//message you win or you lose

function messageCreate(body, message){
    var modMessage = document.createElement('div');
    modMessage.style.display = "block";
    modMessage.setAttribute('id','modal2');
    modMessage.appendChild(message);
    message.style.display = "block";
    body.appendChild(modMessage);
}
//button to reload the page - used for the loss and if one wants to restart after the first move or later
function resetButton(message, body, table){
    if(message === "Play Again"){

        document.getElementById("reset-button").innerHTML = message;

        for(var x=0; x<maxC; x++){
            for(var y=0; y<maxC; y++){
                    document.getElementById(`${x}${y}`).style.backgroundColor = "lightgray";
                    document.getElementById(`${x}${y}`).style.pointerEvents = "none";  
            }
        }
    }else if(message === "Next Level"){

        document.getElementById("reset-button").innerHTML = message;

        for(var x=0; x<maxC; x++){
            for(var y=0; y<maxC; y++){
                    document.getElementById(`${x}${y}`).style.backgroundColor = "lightgreen";
                    document.getElementById(`${x}${y}`).style.pointerEvents = "none";  
            }
        }

        maxC++;
        setCookie("maxC", maxC, 365);
        settingsTable();
    } 
    else {
        var restartButton = document.createElement('button');
        restartButton.style.height = "50px";
        restartButton.style.width = "200px";
        restartButton.style.marginBottom = "30px";
        restartButton.setAttribute("id","reset-button");
        body.appendChild(restartButton);
        document.getElementById("reset-button").innerHTML = message;
    }
    document.getElementById('reset-button').onclick = ()=>{
        if(document.getElementById("message")!=undefined){
            document.getElementById("message").remove();
        }
        counter = 0;
        table.remove();
        document.getElementById('reset-button').remove();
        createTable(body);
    }
}

//function that reset background color of the cells right after clicking.
function resetColor(id){
    var cell = document.getElementById(id);

    setTimeout(() =>{

        if(!lost && !win){
            cell.style.boxShadow = "2px 2px 6px #49bb69";
            cell.style.backgroundColor = "white";
        }
         },1000);
}

//function that checks if a cell has already a number inserted, if not it checks if after it there are
// any possible moves calling the "possibleMoves" function. If so, it returns the id of the previous cell
//and insert the next number in the current cell calling the "insertNumber" function. If there are no possible
//moves after it, it inserts the number in the cell and visualize the message "you lose". If the count is equal
//to the MAX number of cells, it visualize the message "you win".

function checkCell(id, oldId, table, body, message){
    var cell = document.getElementById(id);
    message.setAttribute("id","message");
    var MAXN =maxC*maxC;

    if(cell.innerHTML!='<p style="color: transparent">X</p>'){
        cell.style.backgroundColor = "rgb(241, 95, 95)";
        resetColor(id);
    } else if(allowedMoves(id, oldId)){
        cell.style.border = "1px solid green";
        document.getElementById(oldId).style.border = "1px solid black";        
        
        if(possibleMoves(id)){
            oldId = insertNumber(id);
        }
        else if(!possibleMoves(id)){
            oldId = insertNumber(id);

            if(counter === MAXN ){
                win = true;
                messageCreate(body, message);    
                message.innerHTML = "YOU WIN!";
                resetButton("Next Level", body, table);
            }else{
                lost = true;
                message.innerHTML = "YOU LOSE";
                messageCreate(body, message);
                resetButton("Play Again", body, table);

            }
        }
    } else{
        cell.style.backgroundColor = "lightcoral";
        resetColor(id);
    }
    return oldId; 
};

//this function transform the "id" string into two integer variables i and j, counters for the rows and columns,
//and it returns them.
function idToIJ(oldId){
    oldId = parseInt(oldId);
    var i;
    var j;

    if(oldId>=10){
        j=oldId%10;
        i=(oldId-j)/10;
    } else {
        i=0;
        j=oldId;
    }
    return [i,j];
}

//function that increment the counter and then insert the number into the cell.
function insertNumber(id){
    var cell = document.getElementById(id);

    if(counter!=0){
        cell.style.backgroundColor = "#54d87a";
    }
    cell.innerHTML = `<p style="text-shadow: 3px 3px 10px grey">${++counter}</p>`;
    resetColor(id);
    return id;
}

//this function check if the counters for rows and columns are outside the border of the table.
function checkBorder(i,j){
    if(i<0 || i>=maxC || j<0 || j>=maxC){
        return false;
    }
    return true;
}

//function that returns, from the actual id, if there are any other possible moves and insert this moves into
//an array. 
function possibleMoves(id){
    var ij = idToIJ(id);
    var i, j;
    var possibleMoves = [[0,3],[0,-3],[3,0],[-3,0],[2,-2],[2,2],[-2,-2],[-2,2]];

    var allMoves = new Array();

    for(var x=0; x<maxC; x++){
        for(var y=0; y<maxC; y++){
            if(document.getElementById(`${x}${y}`).style.backgroundColor === "lightgreen"){
                document.getElementById(`${x}${y}`).style.backgroundColor = "white";
            }
        }
    }

    possibleMoves.forEach(element => {
        i = ij[0] + element[0];
        j = ij[1] + element[1];

        if(checkBorder(i,j)){
            if(document.getElementById(`${i}${j}`).innerHTML === '<p style="color: transparent">X</p>'){
                allMoves.push(new Array([i,j]));
                moves.push(new Array([i,j]));
                document.getElementById(`${i}${j}`).style.backgroundColor = "lightgreen";
            }
        }
    });
    if((allMoves.length!=0)){
        return true;
    }
    else return false;
}

//function that checks if the current move is valid (you are moving left, right, up and down of three cells -
//or you are moving diagonally of 2 cells).
function allowedMoves(id, oldId){

    var ij = idToIJ(oldId);
    var i=ij[0];
    var j=ij[1];

    var new_ij = idToIJ(id);
    var new_i = new_ij[0];
    var new_j = new_ij[1];

    function checkTop(){
        if((new_i === i - 3) && (new_j === j)){
            return true;
        }
        return false;
    }
    function checkBottom(){
        if((new_i === i + 3)&&(new_j === j)){
            return true;
        }
        return false;
     }

    function checkLeft(){
        if((new_j === j - 3) && (new_i === i )){
            return true;
        }
        return false;
    }
    function checkRight(){
        if((new_j === j + 3)&&(new_i === i)){
            return true;
        }
        return false;
    }

    if(checkBottom() || checkTop() || //top or bottom
    checkLeft() || checkRight() || //left or right
    ((new_i === i + 2) && (new_j === j - 2)) || ((new_i === i + 2) && (new_j === j + 2)) || //bottom left or bottom right
    ((new_i === i - 2) && (new_j === j - 2)) || ((new_i === i - 2) && (new_j === j + 2) )){ //top left or top right
        if(checkBorder(new_i, new_j)){
            return true;
        }
        return false;
    }                
}
